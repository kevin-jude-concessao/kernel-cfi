CONFIG_MODULE_SIG=n

obj-m += cfi.o 
cfi-objs += module.o 
cfi-objs += cfi_context.o 
cfi-objs += uprobe.o

CFLAGS += -DENABLE_DEBUG -std=c99

all:
	make -C /lib/modules/5.4.16/build M=$(PWD) modules
clean:
	make -C /lib/modules/5.4.16/build M=$(PWD) clean
