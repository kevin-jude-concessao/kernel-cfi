
#include "cfi.h"
#include <linux/kref.h>
#include <linux/sched.h>
#include "linux/err.h"
#include "linux/gfp.h"
#include "linux/kern_levels.h"
#include "linux/pid.h"
#include "linux/rbtree.h"
#include "linux/uprobes.h"
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/slab.h>

static void release_rc_uprobe_list(struct kref *refcount)
{
  struct rc_uprobe_list *list = container_of(refcount, struct rc_uprobe_list, refcount);
  struct uprobe *uprobe;
  struct list_head *node, *_;
  list_for_each_safe(node, _, &list->uprobes.node) {
    uprobe = list_entry(node, struct uprobe, node);
    uprobe->destructor(uprobe);
    list_del(node);
  }
  DEALLOC(list);
}

struct rc_uprobe_list *rc_uprobe_list_new() 
{
  ALLOC_SIMPLE_PTR(list, struct rc_uprobe_list);
  if (!list)
    return ERR_PTR(-ENOMEM);
  kref_init(&list->refcount);
  INIT_LIST_HEAD(&list->uprobes.node);
  return list;
}

struct rc_uprobe_list *rc_uprobe_list_get(struct rc_uprobe_list *list)
{
  kref_get(&list->refcount);
  return list;
}

void rc_uprobe_list_put(struct rc_uprobe_list *uprobe_list)
{
  BUG_ON(kref_read(&uprobe_list->refcount) == 0);
  kref_put(&uprobe_list->refcount, release_rc_uprobe_list);
}

void rc_uprobe_list_insert(struct rc_uprobe_list *list, struct uprobe *uprobe)
{
//  BUG_ON(kref_read(&list->refcount) == 0);
  list_add_tail(&uprobe->node, &list->uprobes.node);
}

struct ctxt *ctxt_new(pid_t pid)
{
  struct task_struct *task;
  struct ctxt *ctxt = NULL;
  task = pid_task(find_vpid(pid), PIDTYPE_TGID);
  if (task == NULL)
    return ERR_PTR(-ESRCH);  
  if (!ALLOC(ctxt))
    return ERR_PTR(-ENOMEM);
  ctxt->pid = pid;
  if (IS_ERR(ctxt->stack = stack_new(4096)))
    cleanup_and_return(ERR_PTR((long)ctxt->stack), kfree(ctxt));
  if (IS_ERR(ctxt->uprobe_list = rc_uprobe_list_new()))
    cleanup_and_return(ERR_PTR((long)ctxt->uprobe_list), stack_delete(ctxt->stack), kfree(ctxt));  
  return ctxt;
}

struct ctxt *ctxt_fork_new(pid_t pid, struct ctxt *parent_ctxt)
{
  struct task_struct *task; 
  struct ctxt *ctxt = NULL;
  task = pid_task(find_vpid(pid), PIDTYPE_TGID);
  if (task == NULL)
    return ERR_PTR(-ESRCH);
  if (!ALLOC(ctxt))
    return ERR_PTR(-ENOMEM);
  ctxt->pid = pid;
  if (IS_ERR(ctxt->stack = stack_copy(parent_ctxt->stack)))
    cleanup_and_return(ERR_PTR((long)(ctxt->stack)), kfree(ctxt));
  ctxt->uprobe_list = rc_uprobe_list_get(parent_ctxt->uprobe_list);
  return ctxt;
}

void ctxt_dtor(struct ctxt *ctxt)
{
  stack_delete(ctxt->stack);
  // c_uprobe_list_put(ctxt->uprobe_list);
  DEALLOC(ctxt);
}

void ctxt_add(struct ctxt *ctxt, struct uprobe *uprobe)
{
  rc_uprobe_list_insert(ctxt->uprobe_list, uprobe);
}

struct ctxt_set *ctxt_set_new()
{
  ALLOC_SIMPLE_PTR(ctxt_set, struct ctxt_set);
  if (!ctxt_set)
    return ERR_PTR(-ENOMEM);
  rwlock_init(&ctxt_set->lock);
  ctxt_set->ctxt_root = RB_ROOT;
  return ctxt_set;
}

void ctxt_set_dtor(struct ctxt_set *ctxt_set)
{
  struct rb_node *node = NULL;
  struct rb_node *next = NULL;
  struct ctxt *ctxt;
  for (node = rb_first(&ctxt_set->ctxt_root); node; node = next) {
    next = rb_next(node);
    ctxt = rb_entry(node, struct ctxt, ctxt_node);
    remove_ctxt(ctxt_set, ctxt);
  }
}

struct ctxt *find_ctxt(struct ctxt_set *ctxt_set, pid_t pid)
{ 
  struct rb_node *node = ctxt_set->ctxt_root.rb_node;
  struct ctxt *ctxt = NULL;
  while (node) {
    ctxt = rb_entry(node, struct ctxt, ctxt_node);
    if (pid < ctxt->pid)
      node = node->rb_left;
    else if (pid > ctxt->pid)
      node = node->rb_right;
    else
      return ctxt;
  }
  return NULL;
}

struct ctxt *ctxt_set_new_pid(struct ctxt_set *ctxt_set, pid_t pid)
{
  struct rb_node **rb_link = &(ctxt_set->ctxt_root.rb_node);
  struct rb_node  *parent  = NULL;
  struct ctxt *ctxt        = NULL;
  struct ctxt *new_ctxt    = NULL;
  while (*rb_link) {
    parent = *rb_link;
    ctxt = rb_entry(parent, struct ctxt, ctxt_node);
    if (pid < ctxt->pid)
      rb_link = &(*rb_link)->rb_left;
    else if (pid > ctxt->pid)
      rb_link = &(*rb_link)->rb_right;
    else
      return ctxt;
  }
  if (IS_ERR(new_ctxt = ctxt_new(pid)))
    return ERR_PTR((long)(new_ctxt));
  rb_link_node(&new_ctxt->ctxt_node, parent, rb_link);
  rb_insert_color(&new_ctxt->ctxt_node, &ctxt_set->ctxt_root);
  return new_ctxt;
}

struct ctxt *ctxt_set_new_pid_fork(struct ctxt_set *ctxt_set, pid_t pid, struct ctxt *parent_ctxt)
{
  struct rb_node **rb_link = &(ctxt_set->ctxt_root.rb_node);
  struct rb_node  *parent  = NULL;
  struct ctxt *ctxt        = NULL;
  struct ctxt *new_ctxt    = NULL;
  while (*rb_link) {
    parent = *rb_link;
    ctxt = rb_entry(parent, struct ctxt, ctxt_node);
    if (pid < ctxt->pid)
      rb_link = &(*rb_link)->rb_left;
    else if (pid > ctxt->pid)
      rb_link = &(*rb_link)->rb_right;
    else
      return ctxt;
  }
  if (IS_ERR(new_ctxt = ctxt_fork_new(pid, parent_ctxt)))
    return ERR_PTR((long)(new_ctxt));
  rb_link_node(&new_ctxt->ctxt_node, parent, rb_link);
  rb_insert_color(&new_ctxt->ctxt_node, &ctxt_set->ctxt_root);
  return new_ctxt;
}

void remove_ctxt(struct ctxt_set *ctxt_set, struct ctxt *ctxt)
{
  rb_erase(&ctxt->ctxt_node, &ctxt_set->ctxt_root);
  ctxt_dtor(ctxt);
}
