#include "asm-generic/int-ll64.h"
#include "asm/page_types.h"
#include "asm/processor.h"
#include "cfi.h"
#include "linux/err.h"
#include "linux/kernel.h"
#include "linux/kref.h"
#include "linux/pagemap.h"
#include "linux/rbtree.h"
#include "linux/sched/task_stack.h"
#include "linux/types.h"
#include "linux/slab.h"
#include "linux/mm.h"
#include "linux/sched/mm.h"
#include "linux/shmem_fs.h"
#include "linux/fs.h"
#include "linux/uaccess.h"
#include "linux/uprobes.h"
#include "linux/ptrace.h"

/**
 * Bytes  Sequence                  Encoding
 * 1      90H                       NOP
 * 2      66 90H                    66 NOP
 * 3      0F 1F 00H                 NOP DWORD ptr [EAX]
 * 4      0F 1F 40 00H              NOP DWORD ptr [EAX + 00H]
 * 5      0F 1F 44 00 00H           NOP DWORD ptr [EAX + EAX*1 + 00H]
 * 6      66 0F 1F 44 00 00H        NOP DWORD ptr [AX + AX*1 + 00H]
 * 7      0F 1F 80 00 00 00 00H     NOP DWORD ptr [EAX + 00000000H]
 * 8      0F 1F 84 00 00 00 00 00H  NOP DWORD ptr [AX + AX*1 + 00000000H]
 */

#define ILLEGAL_INSN_MASK 0x0F0B0F0B0F0B0F0B
#define WORD_MASK(X) (0xFFFFFFFFFFFFFFFF << (8 * X))

#ifdef SINGLE_INSN_NOP
  #error Single instruction nops are reserved for future use. Not supported by uprobes yet.
  #define NOP_MASK_1 (0x90)
  #define NOP_MASK_2 (0x9066)
  #define NOP_MASK_3 (0x001F0F)
  #define NOP_MASK_4 (0x00401F0F)
  #define NOP_MASK_5 (0x0000441F0F)
  #define NOP_MASK_6 (0x0000441F0F66)
  #define NOP_MASK_7 (0x00000000801F0F)
  #define NOP_MASK_8 (0x0000000000841F0F)
#else
  #define NOP_SIZE 1
  #define NOP_MASK_1 (0x90)
  #define NOP_MASK_2 (0x9090)
  #define NOP_MASK_3 (0x909090)
  #define NOP_MASK_4 (0x90909090)
  #define NOP_MASK_5 (0x9090909090)
  #define NOP_MASK_6 (0x909090909090)
  #define NOP_MASK_7 (0x90909090909090)
  #define NOP_MASK_8 (0x9090909090909090)
#endif

static u64 nop_masks[] = {
  [1] = NOP_MASK_1, [2] = NOP_MASK_2, [3] = NOP_MASK_3, [4] = NOP_MASK_4,
  [5] = NOP_MASK_5, [6] = NOP_MASK_6, [7] = NOP_MASK_7, [8] = NOP_MASK_8,
};

#define NOP_SIZE 1

struct address_node {
  u64 address;
  struct rb_node node;
};

struct address_set {
  struct kref refcount;
  struct rb_root root;
};

struct address_set* address_set_new() 
{
  ALLOC_SIMPLE_PTR(set, struct address_set);
  set->root = RB_ROOT;
  kref_init(&set->refcount);
  return set;
}

bool address_set_has(struct address_set *set, u64 address) 
{
  struct rb_node *tnode = set->root.rb_node;
  struct address_node *anode;
  while (tnode) {
    anode = rb_entry(tnode, struct address_node, node);
    if (address < anode->address)
      tnode = tnode->rb_left;
    else if (address > anode->address)
      tnode = tnode->rb_right;
    else return true;
  }
  return false;
}

void address_set_insert(struct address_set *set, u64 address) 
{
  struct rb_node *parent    = NULL;
  struct rb_node **rb_link  = &(set->root.rb_node);
  struct address_node *anode;
  struct address_node *new_anode;
  while (*rb_link) {
    parent = *rb_link;
    anode  = rb_entry(parent, struct address_node, node);
    if (address < anode->address)
      rb_link = &(*rb_link)->rb_left;
    else if (address > anode->address)
      rb_link = &(*rb_link)->rb_right;
    else return;
  }
  ALLOC(new_anode);
  DEBUG("ALLOC");
  rb_link_node(&new_anode->node, parent, rb_link);
  rb_insert_color(&new_anode->node, &set->root);
}

void address_set_remove(struct address_set *set, u64 address) 
{
  struct address_node *anode = NULL;
  struct rb_node      *node  = set->root.rb_node;
  while (node) {
    anode = rb_entry(node, struct address_node, node);
    if (address < anode->address)
      node = node->rb_left;
    else if (address > anode->address) 
      node = node->rb_right;
    else {
      rb_erase(node, &set->root);
      DEALLOC(anode);
      return;
    }
  }
}

void address_set_clear(struct address_set *set) 
{
  struct rb_node *node = NULL;
  struct rb_node *next = NULL;
  for (node = rb_first(&set->root); node; node = next) {
    next = rb_next(node);
    rb_erase(node, &set->root);
  }
}

static void address_set_dtor(struct kref *ref) 
{
  struct address_set *set = container_of(ref, struct address_set, refcount);
  address_set_clear(set);
  DEALLOC(set);
}

struct address_set *address_set_get(struct address_set *set) 
{
  kref_get(&set->refcount);
  return set;
}

void address_set_put(struct address_set *set) 
{
  BUG_ON(kref_read(&set->refcount) == 0);
  kref_put(&set->refcount, address_set_dtor);
}

struct stack {
  u64 size;
  u64 capacity;
  u64 elements[1024];
};

struct stack* stack_new(u64 capacity) 
{
  struct stack *stack = kmalloc(sizeof(stack), GFP_KERNEL);
  if (!stack) 
    return ERR_PTR(-ENOMEM);
  stack->size = 0;
  stack->capacity = 1024;  
  return stack;
}

void stack_delete(struct stack *stack)   { kfree(stack); }
u64  stack_top(struct stack *stack)      { return stack->elements[stack->size - 1]; }
u64  stack_size(struct stack *stack)     { return stack->size; }
u64  stack_capacity(struct stack *stack) { return stack->capacity; }

void stack_push(struct stack *s, u64 element) 
{
  if (s->size != s->capacity) 
    s->elements[s->size++] = element;
  DEBUG("%lld %lld", s->size, s->capacity);
}

void stack_pop(struct stack *stack) 
{
  if (stack->size > 0)
    -- stack->size;
}

struct stack *stack_copy(struct stack *stack) 
{
  struct stack *copy;
  u64 copy_size = sizeof(struct stack) + (stack->capacity - 1);
  if ((copy = kmalloc(copy_size, GFP_KERNEL)) == NULL)
    return ERR_PTR(-ENOMEM);
  memcpy(copy, stack, copy_size);
  return copy;
}

static int copy_from_page(struct page *page, unsigned long vaddr, void *dst, ssize_t nbytes) {
  void *kaddr = kmap_atomic(page);
  memcpy(dst, kaddr + (vaddr & ~PAGE_MASK), nbytes);
  kunmap_atomic(kaddr);
  return 0;
}

static int copy_to_page(struct page *page, unsigned long vaddr, void *src, ssize_t nbytes) {
  void *kaddr = kmap_atomic(page);
  memcpy(kaddr + (vaddr & ~PAGE_MASK), src, nbytes);
  kunmap_atomic(kaddr);
  return 0;
}

static int call_handler(struct uprobe_consumer *consumer, struct pt_regs *registers) 
{
  struct uprobe *uprobe = (struct uprobe*)(consumer);  
  task_pt_regs(current)->sp -= 8;
  DEBUG("--------------------- Call ------------------------");
  DEBUG("Call ID: %d", uprobe->id);
  DEBUG("PREV IP: 0x%lx\n", task_pt_regs(current)->ip);
  DEBUG("Handling call at 0x%llx to 0x%llx :: 0x%llx\n",
        uprobe->c.target_vaddr, uprobe->c.fallthrough_vaddr,
        uprobe->c.target_vaddr - NOP_SIZE);
  task_pt_regs(current)->ip = uprobe->c.target_vaddr - NOP_SIZE;
  DEBUG("CURRENT IP: 0x%lx\n", task_pt_regs(current)->ip);
  stack_push(uprobe->stack_ptr, uprobe->c.fallthrough_vaddr);
  DEBUG("--------------------- EndCall ------------------------");
  return 0;
}

static int return_hander(struct uprobe_consumer *consumer, struct pt_regs *registers) 
{
  struct uprobe *uprobe = (struct uprobe*)(consumer);
  u64 top = stack_top(uprobe->stack_ptr);
  DEBUG("--------------------- Return ------------------------");
  DEBUG("Return ID: %d", uprobe->id);
  stack_pop(uprobe->stack_ptr);
  DEBUG("Anamoly: %lx %llx\n", task_pt_regs(current)->sp, top);
  // WARN(!address_set_has(uprobe->r.return_targets, top), "Stack anamoly detected.");
  DEBUG("RET IP: 0x%lx\n", task_pt_regs(current)->ip);
  task_pt_regs(current)->ip = top - NOP_SIZE;
  task_pt_regs(current)->sp += 8;
  DEBUG("CURRENT IP: 0x%lx\n", task_pt_regs(current)->ip);
  DEBUG("--------------------- EndRet ------------------------");
  return 0;
}

static void uprobe_dtor(void *u)
{
  struct uprobe *uprobe = (struct uprobe*)(u);
  struct probe_location *location = &uprobe->location;
  struct uprobe_consumer *consumer = &uprobe->uprobe_handler;
  struct list_head *entry = &uprobe->node;
  uprobe_unregister(location->inode, location->offset, consumer);
  list_del(entry);  
  DEALLOC(uprobe);
}

static void call_uprobe_dtor(void *u) 
{
  struct uprobe *uprobe = (struct uprobe*)(u);
  uprobe_dtor(uprobe);
}

static void return_uprobe_dtor(void *u)
{
  struct uprobe *uprobe = (struct uprobe*)(u);
  address_set_put(uprobe->r.return_targets);
  uprobe_dtor(uprobe);
}

static struct page *get_mapping_page(struct probe_location const *location) {
  struct page *page;
  struct address_space *as = location->inode->i_mapping;
  if (as->a_ops->readpage)
    page = read_mapping_page(as, location->offset >> PAGE_SHIFT, location->file_ptr);
  else
    page = shmem_read_mapping_page(as, location->offset >> PAGE_SHIFT);
  return page;
}

static int patch_insn_nop_inode(struct page *page, u64 insn_vaddr, u64 insn_size, u64 *orig_word) 
{
  u64 word;
  if (IS_ERR(page))
    return PTR_ERR(page);
  copy_from_page(page, insn_vaddr, &word, sizeof(word));
  if (orig_word)
    *orig_word = word;
  word = (word & WORD_MASK(insn_size)) | nop_masks[insn_size];
  copy_to_page(page, insn_vaddr, &word, sizeof(word));
  return 0;
}

static int patch_nop_insn_inode(struct page *page, u64 insn_vaddr, u64 orig_word) 
{
  if (IS_ERR(page))
    return PTR_ERR(page);
  copy_to_page(page, insn_vaddr, &orig_word, sizeof(orig_word));
  return 0;
}

static int patch_insn_nop_vma(u64 insn_vaddr, u64 insn_size, u64 *orig_word) 
{
  u64 word = 0;
  uint flags = FOLL_FORCE | FOLL_WRITE | FOLL_COW;
  if (access_process_vm(current, insn_vaddr, &word, sizeof(word), FOLL_FORCE) == 0)
    return -EACCES;
  if (orig_word)
    *orig_word = word;
  word = (word & WORD_MASK(insn_size)) | nop_masks[insn_size];
  if (access_process_vm(current, insn_vaddr, &word, sizeof(word), flags) == 0)
    return -EACCES;
  return 0;
}

static int patch_nop_insn_vma(u64 insn_vaddr, u64 orig_word) 
{
  uint flags = FOLL_FORCE | FOLL_WRITE | FOLL_COW;
  if (access_process_vm(current, insn_vaddr, &orig_word, sizeof(insn_vaddr), flags) == 0)
    return -EACCES;
  return 0;
}

static struct uprobe *uprobe_new(enum uprobe_kind kind, u64 vaddr,
                                 int insn_size, struct stack *stack_ptr,
                                 int (*handler)(struct uprobe_consumer *,
                                                struct pt_regs *),
                                 void (*dtor)(void *uprobe)) {
  static int counter;
  
  int result;
  struct probe_location location;
  u64 vma_word;
  u64 inode_word = 0;
  struct page *mapping_page;
  struct uprobe *uprobe = kmalloc(sizeof(struct uprobe), GFP_KERNEL);
  if ((result = find_location(vaddr, &location)) != 0)
    cleanup_and_return(ERR_PTR(result), kfree(uprobe));
  uprobe->uprobe_handler = (struct uprobe_consumer) {
    .handler      = handler,
    .filter       = NULL,
    .ret_handler  = NULL,
    .next         = NULL,
  };
  uprobe->location   = location;
  uprobe->kind       = kind;
  uprobe->vaddr      = vaddr;
  uprobe->destructor = dtor;
  uprobe->stack_ptr  = stack_ptr;
  uprobe->id         = ++counter;
  INIT_LIST_HEAD(&uprobe->node);

  if ((result = patch_insn_nop_vma(vaddr, insn_size, &vma_word)) != 0) {
    cleanup_and_return(ERR_PTR(result), kfree(uprobe));   
  }
  mapping_page = get_mapping_page(&location);
  if ((result = patch_insn_nop_inode(mapping_page, vaddr, insn_size, &inode_word)) != 0) {
    cleanup_and_return(ERR_PTR(result), patch_nop_insn_vma(vaddr, vma_word), put_page(mapping_page), kfree(uprobe));
  }
  put_page(mapping_page);
  if ((result = uprobe_register(location.inode, location.offset, &uprobe->uprobe_handler)) != 0) {
    cleanup_and_return(ERR_PTR(result), patch_nop_insn_inode(mapping_page, vaddr, inode_word), 
                       patch_nop_insn_vma(vaddr, vma_word), put_page(mapping_page), kfree(uprobe));
  } 
  mapping_page = get_mapping_page(&location);
  patch_nop_insn_inode(mapping_page, vaddr, inode_word);
  put_page(mapping_page);
  return uprobe;
}

struct uprobe *call_uprobe_new(u64 vaddr, struct ctxt const *context, u64 target_vaddr, u64 fallthrough_vaddr)
{
  struct uprobe *uprobe = (struct uprobe *)uprobe_new(
      UK_CALL, vaddr, (int)(fallthrough_vaddr - vaddr), context->stack,
      call_handler, call_uprobe_dtor);
  DEBUG("Call ID: %d Fallthrough: %llx  Vaddr: %llx Target: %llx", uprobe->id, fallthrough_vaddr,
        vaddr, target_vaddr);
  uprobe->c.target_vaddr      = target_vaddr;
  uprobe->c.fallthrough_vaddr = fallthrough_vaddr;
  return (struct uprobe *)(uprobe);
}

struct uprobe *return_uprobe_new(u64 vaddr, struct ctxt const *context, struct address_set const *return_targets)
{
  struct uprobe *uprobe = (struct uprobe *)uprobe_new(
      UK_RETURN, vaddr, 1, context->stack, return_hander, return_uprobe_dtor);
  uprobe->r.return_targets = (struct address_set *)return_targets;
  DEBUG("Return ID: %d", uprobe->id);
  return (struct uprobe *)(uprobe);
}

int find_location(u64 vaddr, struct probe_location *location) 
{
  struct mm_struct *mm;
  struct vm_area_struct *vma;
  if ((mm = get_task_mm(current)) == NULL) 
    return -EACCES;
  if ((vma = find_vma(mm, vaddr)) == NULL)
    cleanup_and_return(-EFAULT, mmput(mm));
  if (vma->vm_file == NULL || vma->vm_file->f_inode == NULL)
    cleanup_and_return(-EBADF, mmput(mm));
  location->inode    = vma->vm_file->f_inode;
  location->file_ptr = vma->vm_file;
  location->offset   = (loff_t)((vma->vm_pgoff * PAGE_SIZE) + (vaddr - vma->vm_start));
  mmput(mm);
  return 0;
}