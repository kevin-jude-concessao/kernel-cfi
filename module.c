#include "linux/err.h"
#ifndef CONFIG_UPROBES
  #error kernel-assisted cfi module requires Uprobes support.
#endif

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/sched/task.h>
#include <linux/sched/task_stack.h>
#include <linux/sched/mm.h>
#include <linux/mm.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/ioctl.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/uprobes.h>

#include <linux/spinlock.h>

#include "cfi.h"

static char *device_mode_callback(struct device *device, umode_t *mode) {
  if (mode != NULL)
    *mode = 0666;
  return NULL;
}

static int cfi_device_open(struct inode *inode, struct file *file);
static int cfi_device_release(struct inode *inode, struct file *file);
static long cfi_device_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static long ioctl_read_packets(struct packet_train *packets);
static long ioctl_register_task(void);
static long ioctl_unregister_task(void);
static long ioctl_handle_fork(void);

static dev_t dev;
static struct cdev cfi_device;
static struct class *cfi_device_class;
static struct ctxt_set *ctxt_set;

static struct file_operations cfi_device_file_ops = {
  .owner          = THIS_MODULE,
  .open           = cfi_device_open,
  .unlocked_ioctl = cfi_device_ioctl,
  .release        = cfi_device_release,
};

static int __init cfi_module_init(void) 
{
  if (alloc_chrdev_region(&dev, CFI_DEVICE_MINOR, CFI_DEVICE_COUNT, CFI_DEVICE_NAME) != 0)
    return -ENODEV;  
  if ((cfi_device_class = class_create(THIS_MODULE, CFI_DEVICE_NAME)) == NULL) 
    cleanup_and_return(-ENODEV, unregister_chrdev_region(dev, CFI_DEVICE_COUNT));
  cfi_device_class->devnode = device_mode_callback;
  if (device_create(cfi_device_class, NULL, dev, NULL, CFI_DEVICE_FILE_NAME) == NULL)
    cleanup_and_return(-ENODEV, class_destroy(cfi_device_class),
                       unregister_chrdev_region(dev, CFI_DEVICE_COUNT));
  cdev_init(&cfi_device, &cfi_device_file_ops);
  if (cdev_add(&cfi_device, dev, CFI_DEVICE_COUNT) < 0)
    cleanup_and_return(-ENODEV, device_destroy(cfi_device_class, dev),
                       class_destroy(cfi_device_class),
                       unregister_chrdev_region(dev, CFI_DEVICE_COUNT));
  ctxt_set = ctxt_set_new();
  return 0;
}

static void __exit cfi_module_exit(void) 
{
  ctxt_set_dtor(ctxt_set);
  cdev_del(&cfi_device);
  device_destroy(cfi_device_class, dev);
  class_destroy(cfi_device_class);
  unregister_chrdev_region(dev, CFI_DEVICE_COUNT);
}

static long cfi_device_ioctl(struct file *file, unsigned int command, unsigned long arg) 
{
  int ret = 0;
  switch (command) {
  case IOCTL_READ_EXECUTION_SEQUENCE:    
    ret = ioctl_read_packets((struct packet_train *)arg);
    break; 
  case IOCTL_REGISTER_TASK:
    ret = ioctl_register_task();
    break;  
  case IOCTL_HANDLE_FORK_SYSCALL:
    ret = ioctl_handle_fork();
    break;
  case IOCTL_UNREGISTER_TASK:
    ret = ioctl_unregister_task();
    break;
  default:
    DEBUG_ERROR("invalid ioctl command!!");
    ret = -EINVAL;
  }
  return ret;
}

static long ioctl_read_packets(struct packet_train *ptr) {
  struct packet_train packets;
  struct packet *packets_uptr;
  struct call_info *call_info;
  struct address_set *return_targets;
  u64 *return_insns;
  struct packet *iterator, *end;
  struct call_info *ci_iterator, *ci_end;
  u64 *ri_iterator, *ri_end;
  struct ctxt *ctxt = NULL;
  struct uprobe *call_uprobe, *return_uprobe;
  int count = 0; 

  lock_read(ctxt_set);
  ctxt = find_ctxt(ctxt_set, current->pid);
  unlock_read(ctxt_set);
  if (!ctxt)
    return -ESRCH;

  copy_from_user(&packets, ptr, sizeof(struct packet_train));
  packets_uptr = packets.packets;

  ALLOC_N(packets.packets, packets.length);
  copy_from_user(packets.packets, packets_uptr, sizeof(struct packet) * packets.length);

  iterator = packets.packets;
  end = packets.packets + packets.length;

  while (iterator != end) {  
    if (iterator->callers_n == 0) 
      goto next;
      
    ALLOC_N(call_info, iterator->callers_n);
    ALLOC_N(return_insns, iterator->returns_n);
    
    copy_from_user(call_info, iterator->callers, sizeof(struct call_info) * iterator->callers_n);
    copy_from_user(return_insns, iterator->return_insns, sizeof(u64) * iterator->returns_n);

    ci_iterator = iterator->callers;
    ci_end      = ci_iterator + iterator->callers_n;
    return_targets = address_set_new();

    while (ci_iterator != ci_end) {
      call_uprobe = call_uprobe_new(ci_iterator->call_insn_addr, ctxt, iterator->branch_target, ci_iterator->fallthrough_addr);
      if (!IS_ERR(call_uprobe))
        ctxt_add(ctxt, call_uprobe);

      address_set_insert(return_targets, ci_iterator->fallthrough_addr);
      ++ ci_iterator;
    }

    ri_iterator = iterator->return_insns; 
    ri_end      = ri_iterator + iterator->returns_n;   

    while (ri_iterator != ri_end) {
      return_uprobe = return_uprobe_new(*ri_iterator, ctxt, address_set_get(return_targets));
      if (IS_ERR(return_uprobe))
        address_set_put(return_targets);
      else
        ctxt_add(ctxt, return_uprobe);
      ++ ri_iterator;
    }

    DEALLOC(call_info);
    DEALLOC(return_insns);
  next:
    ++ iterator;
  }

  return 0;
}

static void print_size(void) {
  lock_read(ctxt_set);
  DEBUG("Context size: %ld", count(&ctxt_set->ctxt_root));
  unlock_read(ctxt_set);
}

static long ioctl_register_task(void)
{
  void *result;
  lock_write(ctxt_set);
  if(IS_ERR(result = ctxt_set_new_pid(ctxt_set, current->pid)))
    cleanup_and_return(PTR_ERR(result), unlock_write(ctxt_set));
  unlock_write(ctxt_set);
  print_size();
  return 0;
}

static long ioctl_unregister_task(void)
{
  struct ctxt *ctxt;
  lock_write(ctxt_set);
  ctxt = find_ctxt(ctxt_set, current->pid);
  if (ctxt)
    remove_ctxt(ctxt_set, ctxt);
  unlock_write(ctxt_set);
  print_size();
  return 0;
}

static long ioctl_handle_fork(void)
{
  struct task_struct *parent_task;
  struct ctxt *parent_ctxt;
  rcu_read_lock();
  parent_task = get_task_struct(current->parent);
  rcu_read_unlock();
  lock_write(ctxt_set);
  parent_ctxt = find_ctxt(ctxt_set, parent_task->pid);
  ctxt_set_new_pid_fork(ctxt_set, parent_task->pid, parent_ctxt);
  unlock_write(ctxt_set);
  put_task_struct(parent_task);
  return 0;
}

static int cfi_device_open(struct inode *inode, struct file *file) 
{
  PRETTY_FUNCTION();
  return 0;
}

static int cfi_device_release(struct inode *inode, struct file *file) 
{
  PRETTY_FUNCTION();
  return 0;
}

module_init(cfi_module_init);
module_exit(cfi_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kevin Jude Concessao");
MODULE_DESCRIPTION("kernel-assisted cfi for userspace programs");
MODULE_VERSION("0.4");
