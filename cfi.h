#ifndef CFI_H_
#define CFI_H_

#include "linux/slab.h"
#include <linux/types.h>
#include <linux/kref.h>
#include <linux/list.h>
#include <linux/rbtree.h>
#include <linux/rwlock_types.h>
#include <linux/spinlock.h>
#include <linux/uprobes.h>

#define CFI_DEVICE_NAME       "kernel_cfi"
#define CFI_DEVICE_CLASS_NAME "kernel_cfi"
#define CFI_DEVICE_FILE_NAME  "kernel_cfi"
#define CFI_DEVICE_MINOR 0
#define CFI_DEVICE_COUNT 1
#define __ioctl__

struct breakpoint;
struct packet;

#define ENABLE_DEBUG

struct call_info {
  u64 call_insn_addr;
  u64 fallthrough_addr;
};

struct __ioctl__ packet {
  u64 branch_target;
  u64 callers_n;
  struct call_info *callers;
  u64 returns_n;
  u64 *return_insns;
};

struct packet_train {
  u64 length;
  struct packet *packets;
};

#define IOCTL_MAGIC_VALUE               0x17
#define IOCTL_READ_EXECUTION_SEQUENCE_  0x01
#define IOCTL_REGISTER_TASK_            0x04
#define IOCTL_UNREGISTER_TASK_          0x07
#define IOCTL_HANDLE_FORK_SYSCALL_      0x08

#define IOCTL_READ_EXECUTION_SEQUENCE  _IOR(IOCTL_MAGIC_VALUE, IOCTL_READ_EXECUTION_SEQUENCE_, struct packet_train*)
#define IOCTL_REGISTER_TASK            _IO(IOCTL_MAGIC_VALUE,  IOCTL_REGISTER_TASK_)
#define IOCTL_UNREGISTER_TASK          _IO(IOCTL_MAGIC_VALUE,  IOCTL_UNREGISTER_TASK_)
#define IOCTL_HANDLE_FORK_SYSCALL      _IO(IOCTL_MAGIC_VALUE,  IOCTL_HANDLE_FORK_SYSCALL_)

#define MAX_INSN_SIZE 15

#define ALLOC(var) likely((var = kmalloc(sizeof(*var), GFP_KERNEL)) != NULL)
#define ALLOC_SIMPLE_PTR(var, type)                                            \
  type *var;                                                                   \
  do {                                                                         \
    ALLOC(var);                                                                \
  } while (0)
#define ALLOC_N(var, n) likely((var = kmalloc(sizeof(*var) * n, GFP_KERNEL)) != NULL)
#define ALLOC_SIZE(var, size) likely((var = kmalloc(size, GFP_KERNEL)) != NULL)

#define DEALLOC(var)                                                           \
  do {                                                                         \
    kfree(var);                                                                \
  } while (0)

#define INSN_NOP_MASK

#ifdef ENABLE_DEBUG
  #define DEBUG(...)        printk(KERN_INFO "[DEBUG]: " __VA_ARGS__)
  #define DEBUG_ERROR(...)  printk(KERN_ERR "[ERROR]: " __VA_ARGS__)
  #define PRETTY_FUNCTION() DEBUG("%s", __PRETTY_FUNCTION__)
#else
  #define DEBUG(...)        (0)
  #define DEBUG_ERROR(...)  (0)
  #define PRETTY_FUNCTION() (0)
#endif

#define cleanup_and_return(value, ...) do { (void) (__VA_ARGS__, 0); return value; } while(0)
#define cleanup_and_return_void(...)   do { (void) (__VA_ARGS__, 0); return; } while(0)

enum uprobe_kind { UK_CALL, UK_RETURN };

struct probe_location {
  struct inode *inode; 
  loff_t offset; 
  struct file *file_ptr;
};

struct stack;
struct address_set;

struct address_set *address_set_new(void);
struct address_set *address_set_get(struct address_set *set);
bool address_set_has(struct address_set *set, u64 address);
void address_set_insert(struct address_set *set, u64 address);
void address_set_remove(struct address_set *set, u64 address);
void address_set_clear(struct address_set *set);
void address_set_put(struct address_set *set);

struct uprobe {
  struct uprobe_consumer uprobe_handler;
  int id;
  struct probe_location location;
  enum   uprobe_kind kind;
  u64    vaddr;
  void (*destructor)(void *this_ptr);
  struct stack *stack_ptr;
  union {
    struct {
      u64 target_vaddr;
      u64 fallthrough_vaddr;
    } c;
    struct {
      struct address_set *return_targets;
    } r;
  };
  struct list_head node;
};

#define DTOR_UPROBE(UPROBE) ((UPROBE)->base.destructor)

struct rc_uprobe_list {
  struct kref refcount;
  struct uprobe uprobes;
};

struct ctxt {
  pid_t pid;
  struct stack *stack;
  struct rc_uprobe_list *uprobe_list;
  struct rb_node ctxt_node;
};

typedef int (*uprobe_handler)(struct uprobe_consumer *, struct pt_regs *);

struct uprobe *call_uprobe_new(u64 vaddr, struct ctxt const *context, u64 target_vaddr, u64 fallthrough_vaddr);
struct uprobe *return_uprobe_new(u64 vaddr, struct ctxt const *context, struct address_set const *return_targets);
int find_location(u64 vaddr, struct probe_location *location);

struct rc_uprobe_list *rc_uprobe_list_new(void);
struct rc_uprobe_list *rc_uprobe_list_get(struct rc_uprobe_list *list);
void rc_uprobe_list_put(struct rc_uprobe_list *list);
void rc_uprobe_list_insert(struct rc_uprobe_list *list, struct uprobe *uprobe);

struct stack* stack_new(u64 capacity);
void stack_delete(struct stack *stack);
u64  stack_top(struct stack *stack);
u64  stack_size(struct stack *stack);
u64  stack_capacity(struct stack *stack);
void stack_push(struct stack *stack, u64 element);
void stack_pop(struct stack *stack);
struct stack *stack_copy(struct stack *stack);

struct ctxt *ctxt_new(pid_t pid);
struct ctxt *ctxt_fork_new(pid_t pid, struct ctxt *parent_ctxt);
void ctxt_dtor(struct ctxt *ctxt);
void ctxt_add(struct ctxt *ctxt, struct uprobe *uprobe);


struct ctxt_set {
  rwlock_t lock;
  struct rb_root ctxt_root;
};

static size_t count(struct rb_root *tree)
{
  struct rb_node *node;
  int counter = 0;
  for (node = rb_first(tree); node; node = rb_next(node))
    ++counter;
  return counter;
}

struct ctxt_set *ctxt_set_new(void);
void ctxt_set_dtor(struct ctxt_set *ctxt_set);
struct ctxt *ctxt_set_new_pid(struct ctxt_set *ctxt_set, pid_t pid);
struct ctxt *ctxt_set_new_pid_fork(struct ctxt_set *ctxt_set, pid_t pid, struct ctxt *parent_ctxt);
struct ctxt *find_ctxt(struct ctxt_set *ctxt_set, pid_t pid);
void remove_ctxt(struct ctxt_set *ctxt_set, struct ctxt *ctxt);

#define lock_read(ctxt_set)    read_lock(&((ctxt_set)->lock))
#define lock_write(ctxt_set)   write_lock(&((ctxt_set)->lock))
#define unlock_read(ctxt_set)  read_unlock(&((ctxt_set)->lock))
#define unlock_write(ctxt_set) write_unlock(&((ctxt_set)->lock))

#endif
